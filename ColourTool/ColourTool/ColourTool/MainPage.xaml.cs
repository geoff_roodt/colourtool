﻿using System;
using Xamarin.Forms;

namespace ColourTool
{
    public partial class MainPage : ContentPage
    {
        private ColorModel ColorModel;
        public MainPage()
        {
            InitializeComponent();
            ColorModel = new ColorModel();
            BindingContext = ColorModel;

            ApplyEffects();
        }

        private void BtnRandom_OnClicked(object Sender, EventArgs E)
        {
            if (ColorModel != null)
            {
                ColorModel.RandomColour();
            }
            else
            {
                ColorModel = new ColorModel();
                ColorModel.RandomColour();
            }
        }

        async void BtnSettings_OnClicked(object Sender, EventArgs E)
        {
            try
            {
                var Action = await DisplayActionSheet("Settings", "Cancel", null, "Reset Colours", "Enter Hex", "Enter RGB");
                switch (Action)
                {
                    case "Reset Colours":
                        ColorModel?.Reset();
                        break;
                    case "Enter Hex":
                        ColorModel?.ConvertHexToRgb();
                        break;
                    case "Enter RGB":
                        ColorModel?.ConvertRbgToHex();
                        break;
                }
            }
            catch (Exception Ex)
            {
                ColorModel?.DisplayAlert($"An Error Occured, Please Try Again! \r\n {Ex.Message}");
            }
        }

        private void ApplyEffects()
        {
            SldRed.Effects.Add(Effect.Resolve("Effects.RedSliderEffect"));
            SldGreen.Effects.Add(Effect.Resolve("Effects.GreenSliderEffect"));
            SldBlue.Effects.Add(Effect.Resolve("Effects.BlueSliderEffect"));
        }
    }
}
