﻿using System;
using System.ComponentModel;
using System.Threading;
using Acr.UserDialogs;
using Color = Xamarin.Forms.Color;

namespace ColourTool
{
    public class ColorModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int _Red;
        private int _Green;
        private int _Blue;

        public int Red
        {
            get { return _Red; }
            set
            {
                _Red = value;
                RaisePropertyChanged("Red");
            }
        }
        public int Green
        {
            get { return _Green; }
            set
            {
                _Green = value;
                RaisePropertyChanged("Green");
            }
        }
        public int Blue
        {
            get { return _Blue; }
            set
            {
                _Blue = value;
                RaisePropertyChanged("Blue");
            }
        }

        public string RgbString => $"{Red}, {Green}, {Blue}";
        public string HexString => $"{Red.ToString("X")}{Green.ToString("X")}{Blue.ToString("X")}";

        public Color BoxColour => Color.FromRgb(Red, Green, Blue);

        protected void RaisePropertyChanged(string PropertyName)
        {
            if (PropertyName == "Red" || PropertyName == "Green" || PropertyName == "Blue")
            {
                RaisePropertyChanged("RgbString");
                RaisePropertyChanged("HexString");
                RaisePropertyChanged("BoxColour");
            }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

        public void RandomColour()
        {
            var Rand = new Random();
            Red = Rand.Next(0, 255);
            Green = Rand.Next(0, 255);
            Blue = Rand.Next(0, 255);
        }

        public void Reset()
        {
            Red = 0;
            Green = 0;
            Blue = 0;
        }

        public async void ConvertHexToRgb()
        {
            try
            {
                var Input = await UserDialogs.Instance.PromptAsync("Enter Hex:", "Hex Converter", "Ok", "Cancel", "", InputType.Default, null);
                if (string.IsNullOrWhiteSpace(Input.Text))
                {
                    return;
                }

                var NewColour = Color.FromHex(Input.Text);
                Red = int.Parse(Math.Round(NewColour.R * 255).ToString());
                Green = int.Parse(Math.Round(NewColour.G * 255).ToString());
                Blue = int.Parse(Math.Round(NewColour.B * 255).ToString());
            }
            catch (Exception Ex)
            {
                UserDialogs.Instance.Alert($"Couldn't Convert Hex: \r\n {Ex.Message}", "Error", "Accept");
            }
        }

        public async void ConvertRbgToHex()
        {
            try
            {
                var Input = await UserDialogs.Instance.PromptAsync("Enter RGB Code:", "RGB Converter", "Ok", "Cancel", "", InputType.Default, CancellationToken.None);
                if (string.IsNullOrWhiteSpace(Input.Text))
                {
                    return;
                }
                var RGB = Input.Text.Split(',');
                if (RGB.Length == 3)
                {
                    int R = 0;
                    int G = 0;
                    int B = 0;

                    if (!int.TryParse(RGB[0], out R) && !string.IsNullOrWhiteSpace(RGB[0]))
                    {
                        DisplayAlert("No Valid Integer Supplied For Red!");
                    }
                    if (!int.TryParse(RGB[1], out G) && !string.IsNullOrWhiteSpace(RGB[1]))
                    {
                        DisplayAlert("No Valid Integer Supplied For Green!");
                    }
                    if (!int.TryParse(RGB[2], out B) && !string.IsNullOrWhiteSpace(RGB[2]))
                    {
                        DisplayAlert("No Valid Integer Supplied For Blue!");
                    }

                    var NewColour = Color.FromRgb(R, G, B);
                    Red = int.Parse(Math.Round(NewColour.R * 255).ToString());
                    Green = int.Parse(Math.Round(NewColour.G * 255).ToString());
                    Blue = int.Parse(Math.Round(NewColour.B * 255).ToString());
                }
                else
                {
                    DisplayAlert("Incorrect Arguments Supplied!");
                }

            }
            catch (Exception Ex)
            {
                UserDialogs.Instance.Alert($"{Ex.Message}", "Error", "Accept");
            }
        }

        public void DisplayAlert(string Error)
        {
            UserDialogs.Instance.Alert(Error, "Error", "Accept");
        }

    }
}
