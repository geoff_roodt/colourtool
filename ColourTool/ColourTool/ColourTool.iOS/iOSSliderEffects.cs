using Effects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ResolutionGroupName("Effects")]
[assembly: ExportEffect(typeof(RedSlider), "RedSliderEffect")]
[assembly: ExportEffect(typeof(GreenSlider), "GreenSliderEffect")]
[assembly: ExportEffect(typeof(BlueSlider), "BlueSliderEffect")]

namespace Effects
{
    public class RedSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            var RedSlider = (UISlider)Control;
            RedSlider.ThumbTintColor = UIColor.FromRGB(255, 0, 0);
            RedSlider.MaximumTrackTintColor = UIColor.FromRGB(255, 120, 120);
            RedSlider.MinimumTrackTintColor = UIColor.FromRGB(255, 14, 14);
        }

        protected override void OnDetached()
        {
        }
    }
    public class GreenSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            var GreenSlider = (UISlider)Control;
            GreenSlider.ThumbTintColor = UIColor.FromRGB(0, 255, 0);
            GreenSlider.MaximumTrackTintColor = UIColor.FromRGB(120, 255, 120);
            GreenSlider.MinimumTrackTintColor = UIColor.FromRGB(14, 255, 14);
        }

        protected override void OnDetached()
        {
        }
    }
    public class BlueSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            var BlueSlider = (UISlider)Control;
            BlueSlider.ThumbTintColor = UIColor.FromRGB(0, 0, 255);
            BlueSlider.MaximumTrackTintColor = UIColor.FromRGB(120, 120, 255);
            BlueSlider.MinimumTrackTintColor = UIColor.FromRGB(14, 14, 255);
        }

        protected override void OnDetached()
        {
        }
    }

}