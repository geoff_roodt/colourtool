using Android.Graphics;
using Android.Widget;
using Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("Effects")]
[assembly: ExportEffect(typeof(RedSlider), "RedSliderEffect")]
[assembly: ExportEffect(typeof(GreenSlider), "GreenSliderEffect")]
[assembly: ExportEffect(typeof(BlueSlider), "BlueSliderEffect")]

namespace Effects
{
    public class RedSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            var RedSlider = (SeekBar) Control;
            RedSlider.ProgressDrawable.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Red.ToAndroid(), PorterDuff.Mode.SrcIn));
            RedSlider.Thumb.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Red.ToAndroid(), PorterDuff.Mode.SrcIn));
        }

        protected override void OnDetached()
        {
        }
    }

    public class GreenSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            var GreenSlider = (SeekBar)Control;
            GreenSlider.ProgressDrawable.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Green.ToAndroid(), PorterDuff.Mode.SrcIn));
            GreenSlider.Thumb.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Green.ToAndroid(), PorterDuff.Mode.SrcIn));
        }

        protected override void OnDetached()
        {
        }
    }

    public class BlueSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            var BlueSlider = (SeekBar)Control;
            BlueSlider.ProgressDrawable.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Blue.ToAndroid(), PorterDuff.Mode.SrcIn));
            BlueSlider.Thumb.SetColorFilter(new PorterDuffColorFilter(Xamarin.Forms.Color.Blue.ToAndroid(), PorterDuff.Mode.SrcIn));
        }

        protected override void OnDetached()
        {
        }
    }
}