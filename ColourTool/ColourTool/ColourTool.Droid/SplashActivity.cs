using System.Threading;
using Android.App;
using Android.OS;

namespace ColourTool.Droid
{
    [Activity(Label="Color Tool", Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Thread.Sleep(1000);
            StartActivity(typeof(MainActivity));
        }

    }
}