﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;
using Color = Windows.UI.Color;
using Slider = Windows.UI.Xaml.Controls.Slider;

[assembly: ResolutionGroupName("Effects")]
[assembly: ExportEffect(typeof(RedSlider), "RedSliderEffect")]
[assembly: ExportEffect(typeof(GreenSlider), "GreenSliderEffect")]
[assembly: ExportEffect(typeof(BlueSlider), "BlueSliderEffect")]

namespace Effects
{
    public class RedSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            ((Slider) Control).Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 14, 14));
            ((Slider) Control).Background = new SolidColorBrush(Color.FromArgb(255, 255, 120, 120));
        }

        protected override void OnDetached()
        {
        }
    }

    public class GreenSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            ((Slider)Control).Foreground = new SolidColorBrush(Color.FromArgb(255, 14, 255, 14));
            ((Slider)Control).Background = new SolidColorBrush(Color.FromArgb(255, 120, 255, 120));
        }

        protected override void OnDetached()
        {
        }
    }

    public class BlueSlider : PlatformEffect
    {
        protected override void OnAttached()
        {
            ((Slider)Control).Foreground = new SolidColorBrush(Color.FromArgb(255, 14, 14, 255));
            ((Slider)Control).Background = new SolidColorBrush(Color.FromArgb(255, 120, 120, 255));
        }

        protected override void OnDetached()
        {
        }
    }
}
